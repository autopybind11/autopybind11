import complexTrampoline

s = complexTrampoline.simple()
r = s.hello()

# Gets a map from c++ which corresponds to
# a dict object.
m = s.test()
print(m)
m[2] = 1
print(m.keys())
print(m.values())

print("hello returned " + str(r) + "\n")
if r == 10:
    exit(0)
else:
    exit(-1)
