import sys
import os
sys.path.append(os.getcwd())

from no_file_collision import SimpleTest
from cause_file_collision import SimpleTest2

a = SimpleTest()
assert a.SimpleTest_Check() == 1
b = SimpleTest2()
assert b.SimpleTest2_Check() == 2
