#ifndef simple_h
#define simple_h
#include <iostream>

class simple
{
public:
  simple();
  simple(int var1, int optional=10);
  int hello();
  virtual void hello(std::string name) = 0;
};

class advanced : public simple
{
public:
  advanced();
  void hello(std::string);
};


#endif
