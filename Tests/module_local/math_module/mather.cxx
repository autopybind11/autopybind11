/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt      */

#include "mather.hpp"

adder::adder(int i, int j)
{
    val1 = i;
    val2 = j;
};

void adder::setter(int i, int j)
{
    val1 = i;
    val2 = j;
};

int adder::sum()
{
    return val1+val2;
};


mather::multy::multy(int i, int j)
{
    val1 = i;
    val2 = j;
};

void mather::multy::setter(int i, int j)
{
    val1 = i;
    val2 = j;
};

int mather::multy::multiplier()
{
    int ret_sum = 0;
    adder add = adder();
    for(int i = 0; i<val1; i++)
    {
        add.setter(ret_sum,val2);
        ret_sum=add.sum();
    }
    return ret_sum;
};

