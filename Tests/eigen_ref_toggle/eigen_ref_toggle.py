import os, sys
import unittest

sys.path.append(os.getcwd())

# Taken from smoke_test.py
def whitespace_normalize(s):
    # Naive and ugly, but gets the job done.
    return s.replace(" ", "").replace("\n", "").replace("/", "\\")


# Taken from gen_only.py
def import_fn():
    import toggle_eigen_ref as ter


class EigenRefToggleTest(unittest.TestCase):
    # Test style ported from gen_only.py
    def test_file_compare(self):
        for file in [
            "Eigen_Class_py.cpp",
            "toggle_eigen_ref_free_functions_py.cpp",
        ]:
            with open(file, "r") as f:
                with open("data/" + file, "r") as df:
                    expected = whitespace_normalize(df.read())
                    in_text = whitespace_normalize(f.read())
                    self.assertEqual(expected, in_text)

    def test_no_lib(self):
        # No module means an import error is thrown.
        self.assertRaises(ImportError, import_fn)


if __name__ == "__main__":
    unittest.main()
