# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

import os
import sys
import unittest

# Make it so we search where we are running.
sys.path.append(os.getcwd())

import custom_names_module as cnm
from custom_names_module import nmspc1
from custom_names_module.nmspc1 import nmspc2


class customNameTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def test_base(self):
        cn = nmspc1.base()
        self.assertIn("base_adder", dir(cn))
        self.assertIn("test_", dir(cn))
        self.assertEqual("Base", cn.test_())
        self.assertEqual(5, cn.base_adder(5))
        self.assertEqual(fromBase().test_(), "fromBase")

    def test_custom_name_dir(self):
        t1 = nmspc1.t1D()
        t2 = nmspc2.T2_di()
        t3 = nmspc2.T3_iii()
        self.assertIn("t1_adder", dir(t1))
        self.assertIn("t2_adder", dir(t2))
        self.assertIn("t3_sum", dir(t3))
        self.assertIn("ont_cus", dir(cnm))
        self.assertIn("free_templ_add", dir(cnm))

    def test_T1(self):
        # Test initializations
        self.t1d()
        self.t1i()
        t = fromT1D().test_()
        self.assertEqual(t, "fromT1d")
        self.assertEqual(fromT1D().base_adder(1), 1)

    def test_T2(self):
        self.t2di()
        self.t2dd()

    def test_T3(self):
        self.t3dif()
        self.t3idd()
        self.t3iii()

    def test_free_funcs(self):
        self.assertEqual(cnm.free_add(1.0, 2.0), 3.0)
        self.assertEqual(cnm.free_templ_add(3.0, 4.0), 7.0)
        self.assertEqual(cnm.free_templ_add(1, 2), 3)
        self.assertAlmostEqual(
            cnm.free_add(1.0, 1.0), cnm.free_templ_add(1, 1)
        )

    def test_cnm_enums(self):
        self.assertIn("enum_nt", dir(cnm))
        self.assertIn("unscope", dir(cnm))
        self.assertEqual(cnm.Four, cnm.unscope(0))
        self.assertEqual(cnm.Five, cnm.unscope(1))
        self.assertEqual(cnm.Six, cnm.unscope(2))
        self.assertEqual(cnm.One, cnm.enum_nt(0))
        self.assertEqual(cnm.Two, cnm.enum_nt(1))
        self.assertEqual(cnm.Three, cnm.enum_nt(2))

    def test_overload(self):
        self.assertEqual(cnm.ont_cus(), 0)
        self.assertEqual(cnm.ont_cus(1), 1)
        self.assertEqual(cnm.ont_cus(1, 3), 4)

    def t3dif(self):
        t = nmspc2.T3_dif()
        self.assertEqual(t.test_(), "T3")
        self.assertEqual(t.t3_sum(2.0, 1, 1.0), 4.0)
        self.assertAlmostEqual(
            t.t3_sum(2.0, 2, 4.0), nmspc2.T2_di().t2_adder(4.0, 4)
        )

    def t3idd(self):
        t = nmspc2.T3_idd()
        self.assertEqual(t.test_(), "T3")
        self.assertEqual(t.t3_sum(2, 1.0, 1.0), 4)
        self.assertEqual(t.t3_sum(1, 2.0, 4.4), 7)
        self.assertAlmostEqual(
            t.t3_sum(2, 2.1, 4.0), nmspc2.T2_di().t2_adder(4.0, 4)
        )

    def t3iii(self):
        t = nmspc2.T3_iii()
        self.assertEqual(t.test_(), "T3")
        self.assertEqual(t.t3_sum(2, 1, 1), 4)
        self.assertAlmostEqual(
            t.t3_sum(2, 2, 4), nmspc2.T2_di().t2_adder(4.0, 4)
        )

    def t2di(self):
        t = nmspc2.T2_di()
        self.assertEqual(t.test_(), "T2")
        self.assertEqual(t.t2_adder(2.0, 1), 3.0)
        self.assertAlmostEqual(t.t2_adder(2.0, 1), 3)

    def t2dd(self):
        t = nmspc2.T2_dd()
        self.assertEqual(t.test_(), "T2")
        self.assertEqual(t.t2_adder(2.0, 1.0), 3.0)
        self.assertEqual(t.t2_adder(1.0, 2.0), 3.0)
        self.assertAlmostEqual(
            t.t2_adder(2.0, 1.0), nmspc2.T2_di().t2_adder(1.0, 2)
        )

    def t1d(self):
        t = nmspc1.t1D()
        self.assertEqual(4.0, t.t1_adder(2.0, 2.0))
        self.assertAlmostEqual(4.5, t.t1_adder(2, 2.5))
        self.assertEqual("T1", t.test_())
        # Test Inheritance
        self.assertEqual(1, t.base_adder(1))

    def t1i(self):
        t = nmspc1.t1I()
        self.assertEqual(4, t.t1_adder(2, 2))
        self.assertAlmostEqual(4, t.t1_adder(2, int(2.5)))
        self.assertEqual("T1", t.test_())
        # Test Inheritance
        self.assertEqual(1, t.base_adder(1))


class fromBase(nmspc1.base):
    def __init__(self, other=None):
        if not other:
            nmspc1.base.__init__(self)
        else:
            nmspc1.base.__init__(self, other)

    def test_(self):
        return "fromBase"


class fromT1D(nmspc1.t1D):
    def __init__(self, other=None):
        if not other:
            nmspc1.t1D.__init__(self)
        else:
            nmspc1.t1D.__init__(self, other)

    def test_(self):
        return "fromT1d"


if __name__ == "__main__":
    unittest.main()
