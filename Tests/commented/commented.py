# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt
import os
import sys
import unittest

# Make it so we search where we are running.
sys.path.append("%s/commented" % os.getcwd())
sys.path.append("%s/uncommented" % os.getcwd())

import simpleTest as st
import uncommentedTest as ut

simple = st.simple()
usimple = ut.uSimple()


class commentedTests(unittest.TestCase):
    def testNoComments(self):
        self.assertEqual(None, usimple.__doc__)
        # Method has built-in docstring. Ensure our comment isn't in it
        self.assertNotIn("/** method comment */", usimple.hello.__doc__)

    def testClassComments(self):
        self.assertEqual("/** class comment */", simple.__doc__)

    def testMethodComments(self):
        self.assertIn("/** method comment */", simple.hello.__doc__)
        self.assertIn("/** doc comment */", simple.invisible.__doc__)
        self.assertIn(
            """/** Still need a comment 
on multiple lines */""",
            simple.doubleLine.__doc__,
        )
        self.assertIn(
            """/*! unique holder comment 
across multiple lines */""",
            simple.get_vec.__doc__,
        )

    def testEnumComments(self):
        self.assertIn("/** enum comment */", simple.Values.__doc__)

    # Field entries do not seem to be able to have the doc strings
    # overwritten.  Keep this here for later when they might,
    # pygccxml passes the strings as expected.

    # def testVariableComments(self):
    #    import pdb; pdb.set_trace()
    #    self.assertIn("/*! variable comment with multiple lines */", simple.field_obj.__doc__)


if __name__ == "__main__":
    testRunner = unittest.main(__name__, argv=["main"], exit=False)
    if not testRunner.result.wasSuccessful():
        sys.exit(1)
