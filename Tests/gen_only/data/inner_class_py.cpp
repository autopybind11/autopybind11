#include <pybind11/pybind11.h>
#include <pybind11/iostream.h>
#include <pybind11/stl.h>
#include "inner.hpp"


using namespace inner;




namespace py = pybind11;
void apb11_gen_only_module_inner_class_py_register(py::module &m)
{

  py::class_<inner_class> Pyinner_class(m, "inner_class");


    Pyinner_class.def(py::init<>())
    .def(py::init<inner_class const &>(),py::arg("arg0"))



    ;
}
