# Pull all existing tests

from unittest import TestLoader, TextTestRunner
import sys
import os

file_path = os.path.dirname(__file__)
def non_template():
    suite = TestLoader().discover(os.path.abspath(os.path.join(file_path, 'tests', 'non_template')), "*Tests.py")
    runner = TextTestRunner()
    result = runner.run(suite)
    if not result.wasSuccessful():
        sys.exit(1)
def template():

    suite = TestLoader().discover(os.path.abspath(os.path.join(file_path, 'tests', 'template')), "*Tests.py")
    runner = TextTestRunner()
    result = runner.run(suite)
    if not result.wasSuccessful():
        sys.exit(1)

non_template()
# Since they're named the same, lets force the "reload" by deleting the non-template versions
for module in ['basicClassTests',
               'py_classes',
               'trampolineCopyConstructorTests',
               'trampolineTests']:
    if module in sys.modules:
        del sys.modules[module]
template()
