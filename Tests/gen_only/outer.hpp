/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */
#ifndef OUTER_HPP
#define OUTER_HPP

class outer_class
{
};

inline void outer_fxn(){}

#endif