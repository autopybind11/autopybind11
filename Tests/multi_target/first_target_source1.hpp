// Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
// file Copyright.txt

#ifndef FIRST_TARGET_SOURCE1_HPP
#define FIRST_TARGET_SOURCE1_HPP

#include "subdir/first_target_source2.hpp"

class first_target_class
{
public:
  first_target_class() = default;
  #ifdef FLAG1
    void flag1_defined(){}
  #else
    void flag1_not_defined(){}
  #endif

  // Prove that the include worked
  void call_free_fxn() { first_target_fxn(); }
};

#endif