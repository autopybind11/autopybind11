#ifndef simple_h
#define simple_h
#include <array>

class simple
{
public:
  simple();
  int hello();
  void print_four(std::array<double,4> numArray = {1,2,3,4});
  void print_one(double val = 1);
};

#endif
