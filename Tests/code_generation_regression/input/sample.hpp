class A {};

class B {
public:
  A Something() const { return A{}; }
  A Alpha() const { return A{}; }
  A Beta() const { return A{}; }
};
