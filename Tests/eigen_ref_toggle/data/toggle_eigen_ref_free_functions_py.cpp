#include <pybind11/pybind11.h>
#include <pybind11/iostream.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>
#include "eigen_no_ref.hpp"

namespace py = pybind11;

void apb11_toggle_eigen_ref_toggle_eigen_ref_free_functions_py_register(py::module &m)
{

  
  m.def("transpose", static_cast<::Eigen::MatrixXd (*)( ::Eigen::MatrixXd )>(&transpose), py::arg("mat"));
  m.def("transpose_in_place", static_cast<void (*)( ::Eigen::MatrixXd )>(&transpose_in_place), py::arg("mat"));
}
