#include <iostream>
#include "simple.h"

simple::simple()
{
}


int simple::hello()
{
  std::cout << "hello\n";
  return 10;
}

template<>
aliasClass<simple>::aliasClass()
{
}

template<>
aliasDoubleClass<new_alias, simple>::aliasDoubleClass()
{
}
