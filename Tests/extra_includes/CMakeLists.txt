# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt
cmake_minimum_required(VERSION 3.15)
project(extra_includes CXX)

find_package(AutoPyBind11)
autopybind11_fetch_build_pybind11(PYBIND11_DIR ${PYBIND11_SRC_DIR})

add_library(mylib INTERFACE)
target_include_directories(mylib INTERFACE ${CMAKE_CURRENT_SOURCE_DIR})

set(EXTRA_INCLUDES ${CMAKE_CURRENT_SOURCE_DIR}/extra_inc)

autopybind11_add_module(myModule 
                        YAML_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/wrapper_input.yml
                        DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
                        LINK_LIBRARIES mylib
                        INCLUDE_DIRS ${EXTRA_INCLUDES}
                        )
