#ifndef simple_h
#define simple_h

enum enumPlain {
    val0,
    val1,
    val2,
    val3
};

enum enumCustVal {
    zero,
    one,
    two,
    three
};
enum enumWithExport {
    exportVal0,
    exportVal1,
    exportVal2,
    exportVal3
};
enum enumWithBoth {
    both0,
    both1,
    both2,
    both3
};
#endif
