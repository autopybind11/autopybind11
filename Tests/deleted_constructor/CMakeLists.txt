cmake_minimum_required(VERSION 3.15)
project(simple CXX)

find_package(AutoPyBind11)
autopybind11_fetch_build_pybind11(PYBIND11_DIR ${PYBIND11_SRC_DIR})
add_library(simpleLib simple.cxx)
target_compile_options(simpleLib PUBLIC "--std=c++11")
set_property(TARGET simpleLib PROPERTY POSITION_INDEPENDENT_CODE ON)
autopybind11_add_module("deleted_constructor_module" YAML_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/simple_wrap.yml
                        DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
                        LINK_LIBRARIES simpleLib)
